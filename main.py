import os
import io
import mlflow.pyfunc
from dotenv import load_dotenv
import pandas as pd
from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

load_dotenv()

app = FastAPI()
templates = Jinja2Templates(directory="templates")  # ����� � ��������� HTML

# ������ ���������� ��������� ��� MLflow
mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))

# �������� ������ �� MLflow
model_name = "model_e42327fceafa42a5a793f4df177ee089"
model_version = 1
model_uri = f"models:/{model_name}/{model_version}"
model = mlflow.pyfunc.load_model(model_uri)

@app.get("/", response_class=HTMLResponse)
def read_root():
    return """
    <html>
        <head>
            <title>ML Model Serving</title>
        </head>
        <body>
            <h1>Upload CSV for Prediction</h1>
            <form action="/predict" enctype="multipart/form-data" method="post">
                <input name="file" type="file">
                <input type="submit">
            </form>
        </body>
    </html>
    """

@app.post("/predict")
async def predict(file: UploadFile = File(...)):
    try:
        # ��������� ����������� CSV ���� � DataFrame
        contents = await file.read()
        df = pd.read_csv(io.StringIO(contents.decode('utf-8')), index_col="customerID")

        # ��������� ������������ ������
        predictions = model.predict(df)

        # ������� ������������
        return {"predictions": predictions.tolist()}

    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Error during prediction: {str(e)}")
